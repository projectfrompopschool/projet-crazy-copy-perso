import {StyleSheet, Image,Pressable,View,Modal,Text} from 'react-native';
import { useState } from 'react';
import {Picker} from '@react-native-picker/picker';
import React from 'react';
import { useNavigation } from '@react-navigation/native';


const ButtonParams = ({image}) => {
  const navigation = useNavigation();

    const commentJouer = () => {
      navigation.navigate('Tuto');
    };

    const Prenium = () => {
      navigation.navigate('Prenium');
    };

    const [modalVisible, setModalVisible] = useState(false);
    state = {modalVisible: false,};
    const [selectedLanguage, setSelectedLanguage] = useState('France');

    console.log(modalVisible);
    return (<>
    <Pressable onPress={() => setModalVisible(true)}>
        <Image style={{width: 60, height:60}} source={image}/>
    </Pressable>

  <View style={styles.centeredView}>
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(false);
      }}>
      <View style={styles.centeredView}>
        
        <View style={styles.modalView}>
            <Pressable
              style={[styles.buttonCroix]}
              onPress={() => setModalVisible(false)}>
              <Image style={{width: 60, height:60}} source={require('../assets/images/fonds/newbouttoncroix.png')}/>
            </Pressable>

          <Text style={styles.textReglage}>RÉGLAGES</Text>

          <Pressable
            style={[styles.buttonAcheter]}
            onPress={Prenium}>
            <Text style={styles.textStyle}>TELECHARGER la VERSION PREMIUM</Text>
          </Pressable>

          <Pressable
            style={[styles.buttonCommentjouer]}
            onPress={commentJouer}>
            <Text style={styles.textStyle}>COMMENT JOUER ?</Text>
          </Pressable>

          <View style={styles.divLangue}>
            <Text style={styles.textStyle}>LANGUES</Text>
            <View style={styles.divSelect}>
              <Picker
                style={styles.selectPicker}
                selectedValue={selectedLanguage}
                onValueChange={(itemValue, itemIndex) =>
                  setSelectedLanguage(itemValue)
                }>
                <Picker.Item label="France" value="France" />
                <Picker.Item label="English" value="English" />
                <Picker.Item label="Mandarin" value="Mandarin" />
                <Picker.Item label="Espagnol" value="Espagnol" />
                <Picker.Item label="Arabe" value="Arabe" />
                <Picker.Item label="Russe" value="Russe" />
                <Picker.Item label="Coréen" value="Coréen" />
              </Picker>
            </View>
          </View>

          <View style={styles.divPartager}>              
            <Text style={styles.textStylePartager}>Partage sur tes réseaux !</Text>
            <Pressable
              style={[styles.buttonPartager]}
              onPress={() => setModalVisible(false)}>
              <Image style={{width: 50, height:70}} source={require("../assets/images/fonds/newbouttonpartager.png")}></Image>
            </Pressable>
          </View>
   

        </View>
      </View>
    </Modal>
  </View>
  </>) 
}

const styles = StyleSheet.create({
    centeredView: {
      paddingTop: "10%",
      justifyContent: 'center',
      marginTop: "5%",
    },
    buttonCommentjouer: {
      marginVertical:  "2%"
    },
    divLangue: {
      marginVertical:  "2%"
    },
    divPartager: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: "29%",
      paddingVertical: "10%"
    },
    textStylePartager: {
      fontFamily: 'Poppins',
      fontSize: 20,
    },
    modalView: {
      marginTop: "15%",
      margin: "5%",
      backgroundColor: 'white',
      borderRadius: 30,
      borderColor: 'black',
      borderWidth: 7,
      alignItems: 'center',
    },
    buttonAcheter: {
      marginLeft: "-4%",
      marginVertical:  "2%"
    },
    divSelect: {
      borderWidth: 2,
      borderColor: 'black',
      width: "100%",
    },
    selectPicker: {
      paddingHorizontal: "44%",
      borderColor: 'black',
      fontFamily: 'Poppins'
    },
    buttonCroix: {
      marginLeft: "86%",
      marginTop: "-9%",
    },
    buttonOpen: {
      backgroundColor: '#F194FF',
    },
    buttonPartager: {
      paddingLeft: "15%"
    },
    textStyle: {
      color: 'black',
      fontFamily: 'Poppins',
      fontSize: 30,
      paddingLeft: "0.1%",
    },
    textReglage: {
      marginBottom: "10%",
      textAlign: 'center',
      fontFamily: 'Poppins-bold',
      fontSize: 45,
      color: 'black',
    },
  });

export default ButtonParams;