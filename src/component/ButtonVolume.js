import {StyleSheet, Image,Pressable,View,Modal,Text} from 'react-native';
import { useEffect, useState } from 'react';
import ImgButtonVolume from "../../src/assets/images/fonds/volume.png";
import AsyncStorage from '@react-native-async-storage/async-storage';
import LogoVolume from "../../src/assets/images/fonds/volume.png"
import LogoVolumeMute from "../../src/assets/images/fonds/volumeArret.png"



const ButtonVolume = ({image}) => {
    const [etatSong, setEtatSong] = useState(true);
    const getUser = async () => {
        try {
            const userData = JSON.parse(await AsyncStorage.getItem("etatSong"))
            setEtatSong(userData);
        } catch (error) {
            console.log(error);
        }
    };
    const storeUser = async () => {
        try {
            await AsyncStorage.getItem('etatSong').then((value) => {
                if(value == JSON.stringify(true)){
                    AsyncStorage.setItem("etatSong", JSON.stringify(false));
                    setEtatSong(false)
                }else{
                    AsyncStorage.setItem("etatSong", JSON.stringify(true));
                    setEtatSong(true)
                }
            });
            console.log(await AsyncStorage.getItem('etatSong'))
        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {
        getUser();
    }, [])

    return (
    <>

        <Pressable onPress={() => {storeUser()}}>
            <Image style={{width: 25, height:25}} source={etatSong ? LogoVolume : LogoVolumeMute}/>
            {/* <Image style={{width: 25, height:25}} source={image}/> */}
        </Pressable>

    </>)
};

export default ButtonVolume;