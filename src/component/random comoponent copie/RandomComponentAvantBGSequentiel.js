import React, { useState, useEffect,useRef } from 'react';
import { Image, ImageBackground, Pressable, StyleSheet, Text, View } from 'react-native';
import lettre from '../services/LettresService';
import themes from '../services/ThemesService';
import BackGroundThemeAleatoire from '../services/BackGroundThemeAleatoire';
import BackGroundLettreAleatoire from '../services/BackGroundLettreAleatoire';
import * as Speech from 'expo-speech';


const RandomComponent = ({ navigation }) => {
  const [backGroundTheme, setBackGroundTheme] = useState(null);
  const [backGroundLettre, setBackGroundLettre] = useState(null);
  const [themeNom, setThemeNom] = useState(null);
  const [themeImage, setThemeImage] = useState(null);
  const chiffreL = Math.floor(Math.random() * 24);
  const chiffreT = Math.floor(Math.random() * 14);
  const url  = lettre[chiffreL]
  const colors = ['#18D5FF', '#FF0000', '#FFE602'];
  const randomColor = colors[Math.floor(Math.random() * colors.length)];
  
  useEffect(() => {
    const vocalTheme = themes[chiffreT]["parler"]
    const themeNom = themes[chiffreT]["theme"]
    const themeImage = themes[chiffreT]["image"]
    setThemeNom(themeNom);
    setThemeImage(themeImage);
    Speech.speak(vocalTheme, {
      language: "fr-FR",
      pitch: 1.5,
      rate: 1
    });
  }, []);

  useEffect(() => {
    setBackGroundTheme(BackGroundThemeAleatoire[randomColor]);
    setBackGroundLettre(BackGroundLettreAleatoire[randomColor]);
  }, [randomColor]);

// console.log(themeNom);

  return (
    <View style={[styles.containerParent, {backgroundColor: randomColor} ]}>
      <View style={styles.divThemeNom}>
        <Text style={[styles.textThemeNom]}>{themeNom}</Text>
      </View>

      <View style={[styles.divThemeAleatoire]}>
        <ImageBackground source={backGroundTheme} style={[styles.BackGroundThemeAleatoire, {width: 300, height: 300}]}>
          <Image source={themeImage} style={[styles.imageThemeAleatoire, {width: 200, height: 200}]}/>
        </ImageBackground>
      </View>

      <View style={styles.containerLettreAleatoire}>
        <View style={styles.divLettreAleatoire}>
          <ImageBackground source={backGroundLettre} style={[styles.backgroundLettreAleatoire, {width: 140, height: 130}]}>
            <Image source={url} style={[styles.imageLettreAleatoire, {width: 80, height:80}]}/>
          </ImageBackground>
        </View>

        <View style={styles.divLettreAleatoireFleche}>
          <Pressable style={styles.buttonNextGame} onPress={() =>  navigation.push('RandomComponent')}>
            <Image style={[styles.imageButtonNextGame, {width: 80, height:80}]} source={require('../assets/images/fonds/nextbutton.png')} />
          </Pressable>
        </View>
      </View>

      <View style={[styles.divReglesGage]}>
        <Pressable style={styles.buttonReglesGage} onPress={() => {}}>
          <Text style={styles.textReglesGage}>5 points gagnés =</Text>
          <Text style={styles.textReglesGage}>tu donnes un gage à qui tu veux !</Text>
        </Pressable>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containerParent: {
    flex: 1,
  },
  divThemeNom: {
    marginTop: "25%",
    height: "18%"
  },
  textThemeNom: {
    fontFamily: "Stora",
    textAlign: "center",
    fontSize: 50,
    color: "white",
  },
  divThemeAleatoire: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  BackGroundThemeAleatoire: {
    paddingVertical: "30%",
    justifyContent: 'center',
    // alignContent: 'center',
    alignItems: 'center'
  },
  containerLettreAleatoire: {
    flexDirection: "row",
    justifyContent: "space-around"
  },
  backgroundLettreAleatoire: {
    justifyContent: 'center',
    // alignContent: 'center',
    alignItems: 'center'
  },
  divLettreAleatoireFleche: {
    justifyContent: 'center',
    // alignContent: 'center',
    alignItems: 'center'
  },
  divReglesGage: {
    backgroundColor: "black",
    justifyContent: "flex-end",
    // flex: 1,
    marginTop: "6.5%"
  },
  textReglesGage: {
    textAlign: "center",
    color: "white",
    fontSize: 20,
    fontWeight: 900,
    paddingVertical: "1%"
  }
});

export default RandomComponent;