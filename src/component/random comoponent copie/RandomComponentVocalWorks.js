import React, { useState, useEffect } from 'react';
import { Image, ImageBackground, Pressable, StyleSheet, Text, View } from 'react-native';
import lettre from '../services/LettresService';
import themes from '../services/ThemesService';
import BackGroundThemeAleatoire from '../services/BackGroundThemeAleatoire';
import BackGroundLettreAleatoire from '../services/BackGroundLettreAleatoire';
import * as Speech from 'expo-speech';


const RandomComponent = ({ navigation }) => {
  const [backgroundColor, setBackgroundColor] = useState(null);
  const [backGroundTheme, setBackGroundTheme] = useState(null);
  const [backGroundLettre, setBackGroundLettre] = useState(null);
  // const [vocal, setVocal] = useState(null);

  const chiffreL = Math.floor(Math.random() * 24);
  const chiffreT = Math.floor(Math.random() * 14);
  const themeNom = themes[chiffreT]["theme"]
  const themeImage = themes[chiffreT]["image"]
  const vocalTheme = themes[chiffreT]["parler"]
  const url  = lettre[chiffreL]

  const colors = ['#18D5FF', '#FF0000', '#FFE602'];


  // useEffect(() => {
  //   setVocal(vocalTheme);
  //   console.log(vocalTheme);
  //   console.log(themeNom);
  //   // console.log(vocal)
  // },[]);


// useEffect(() => {
//   setVocal(vocalTheme);

// },[])

  //   const handleVoice = (ttsText) => {
  //   // Tts.speak(ttsText);
  //   Speech.speak(ttsText, {
  //         // voice: "com.apple.speech.synthesis.voice.Fred",
  //         language: "fr-FR",
  //         pitch: 1.5,
  //         rate: 1
  //   });
  // };


  // useEffect(() => {
  //   const randomColor = colors[Math.floor(Math.random() * colors.length)];
  //   setBackgroundColor(randomColor);
  // }, []);

  useEffect(() => {
    if (backgroundColor) {
      setBackGroundTheme(BackGroundThemeAleatoire[backgroundColor]);
      setBackGroundLettre(BackGroundLettreAleatoire[backgroundColor]);
    }
  }, [backgroundColor]);

  
  // useEffect(() => {
  //   if (backgroundColor) {
  //     setBackGroundLettre(BackGroundLettreAleatoire[backgroundColor]);
  //   }
  // }, [backgroundColor]);

  useEffect(() => {
    Speech.speak(vocalTheme, {
      // voice: "com.apple.speech.synthesis.voice.Fred",
      language: "fr-FR",
      pitch: 1.5,
      rate: 1
    });
    console.log(vocalTheme)
  }, [vocalTheme]);

  // console.log(backGroundTheme);

  return (
    <View style={[styles.containerParent, { backgroundColor }]}>
      <View style={styles.divThemeNom}>
        <Text style={[styles.textThemeNom]}>{themeNom}</Text>
      </View>

      <Text onPress={() => handleVoice('prend la pillule Rouge Néo')}>
        APPUI
      </Text>

      <View style={[styles.divThemeAleatoire]}>
        <ImageBackground source={backGroundTheme} style={[styles.BackGroundThemeAleatoire, {width: 300, height: 300}]}>
          <Image source={themeImage} style={[styles.imageThemeAleatoire, {width: 200, height: 200}]}/>
        </ImageBackground>
      </View>

      <View style={styles.containerLettreAleatoire}>
        <View style={styles.divLettreAleatoire}>
          <ImageBackground source={backGroundLettre} style={[styles.backgroundLettreAleatoire, {width: 140, height: 130}]}>
            <Image source={url} style={[styles.imageLettreAleatoire, {width: 80, height:80}]}/>
          </ImageBackground>
        </View>

        <View style={styles.divLettreAleatoireFleche}>
          {/* <Pressable style={styles.buttonNextGame} onPress={() => {handleVoice(vocalTheme), navigation.push('RandomComponent')}}> */}
          <Pressable style={styles.buttonNextGame} onPress={() =>  navigation.push('RandomComponent')}>
            <Image style={[styles.imageButtonNextGame, {width: 80, height:80}]} source={require('../assets/images/fonds/nextbutton.png')} />
          </Pressable>
        </View>
      </View>

      <View style={[styles.divReglesGage]}>
        <Pressable style={styles.buttonReglesGage} onPress={() => {}}>
          <Text style={styles.textReglesGage}>5 points gagnés =</Text>
          <Text style={styles.textReglesGage}>tu donnes un gage à qui tu veux !</Text>
        </Pressable>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containerParent: {
    flex: 1,
  },
  divThemeNom: {
    marginTop: "25%",
    height: "18%"
  },
  textThemeNom: {
    fontFamily: "Stora",
    textAlign: "center",
    fontSize: 50,
    color: "white",
  },
  divThemeAleatoire: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  BackGroundThemeAleatoire: {
    paddingVertical: "30%",
    justifyContent: 'center',
    // alignContent: 'center',
    alignItems: 'center'
  },
  containerLettreAleatoire: {
    flexDirection: "row",
    justifyContent: "space-around"
  },
  backgroundLettreAleatoire: {
    justifyContent: 'center',
    // alignContent: 'center',
    alignItems: 'center'
  },
  divLettreAleatoireFleche: {
    justifyContent: 'center',
    // alignContent: 'center',
    alignItems: 'center'
  },
  divReglesGage: {
    backgroundColor: "black",
    justifyContent: "flex-end",
    // flex: 1,
    marginTop: "6.5%"
  },
  textReglesGage: {
    textAlign: "center",
    color: "white",
    fontSize: 20,
    fontWeight: 900,
    paddingVertical: "1%"
  }
});

export default RandomComponent;