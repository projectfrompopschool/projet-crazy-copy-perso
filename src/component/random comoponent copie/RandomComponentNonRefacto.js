import React, { useState, useEffect }  from 'react';
import {Image, ImageBackground, Pressable, requireNativeComponent, StyleSheet, Text, View} from 'react-native';
import lettre from '../services/LettresService'
import themes from '../services/ThemesService'
import AsyncStorage from '@react-native-async-storage/async-storage';
import BackGroundThemeAleatoire from '../services/BackGroundThemeAleatoire';
import BackGroundLettreAleatoire from '../services/BackGroundLettreAleatoire';

const RandomComponent = ({navigation}) => {
    const chiffreL = Math.floor(Math.random() * 24);
    const chiffreT = Math.floor(Math.random() * 4);
    const chiffreBackGroundTheme = Math.floor(Math.random() * 3);
    const chiffreBackGroundLettre = Math.floor(Math.random() * 3);
    const themeNom = themes[chiffreT]["theme"]
    const themeImage = themes[chiffreT]["image"]
    const url  = lettre[chiffreL]
    const urlBackGroundTheme = BackGroundThemeAleatoire[chiffreBackGroundTheme];
    const urlBackGroundLettre = BackGroundLettreAleatoire[chiffreBackGroundLettre];

    const colors = ['#18D5FF', '#FF0000', '#FFE602'];
    const randomIndex = Math.floor(Math.random() * colors.length);
    // const [colorIndex, setColorIndex] = useState(0);
    const [backgroundColor, setBackgroundColor] = useState(colors[randomIndex]);

    // const handleNextColor = () => {
    //     const nextIndex = (colorIndex + 1) % colors.length;
    //     setColorIndex(nextIndex);
    //     setBackgroundColor(colors[nextIndex]);
    //     AsyncStorage.setItem('colorIndex');
    // };

    // const ChangeColoroBackGround = () => {
    //     const colors = ['red', 'green', 'blue', 'yellow', 'purple'];
    //     const randomColor = colors[Math.floor(Math.random() * colors.length)];
    //     setBackgroundColor(randomColor);
    //     console.log(randomColor);
    //   };
    // useEffect(() => {
    //     const retrieveColorIndex = async () => {
    //       try {
    //         const index = await AsyncStorage.getItem('colorIndex');
    //         if (index !== null) {
    //           setColorIndex(parseInt(index));
    //         }
    //       } catch (e) {
    //         console.error(e);
    //       }
    //     };
    //     retrieveColorIndex();
    // }, []);

    return (
    <> 
    <View style={[styles.divContainer, { backgroundColor }]}>
        <View  style={styles.container}>
            <Text style={[styles.lettre, styles.themeNom]}>{themeNom}</Text>
        </View>

        <View style={[styles.container, styles.imageAleatoireContainer]}>
            <ImageBackground source={urlBackGroundTheme}>
                <Image source={themeImage} style={[styles.lettre,styles.imageAleatoire]} />
            </ImageBackground>
        </View>

        <View  style={[styles.container, styles.lettreAleatoire]}>
            <View>
                <ImageBackground source={urlBackGroundLettre}>
                    <Image source={url} style={styles.lettre} />
                </ImageBackground>
            </View>

            <View>
            {/* <Pressable style={styles.button} onPress={() => {ChangeColoroBackGround(); navigation.push('RandomComponent'); }}> */}
            <Pressable style={styles.button} onPress={() => {navigation.push('RandomComponent')}}>
            {/* <Pressable style={styles.button} onPress={ChangeColoroBackGround}> */}

            {/* <Pressable style={styles.button} onPress={() => {handleNextColor(); navigation.push('RandomComponent'); }}> */}
                <Image source={require('../assets/images/fonds/nextbutton.png')} />
            </Pressable>
            </View>
        </View>

        <View style={[styles.container, styles.containerGage]}>
            <Pressable style={styles.button} onPress={() => {}}>
                    <Text style={styles.text}>5 points gagnés =</Text>
                    <Text style={styles.text}>tu donnes un gage à qui tu veux !</Text>
            </Pressable>
        </View>
    </View>

    </>
    );
};
const styles = StyleSheet.create({
    container:{
    //   backgroundColor:"#18D5FF"
    },
    imageAleatoireContainer: {
        flex:1,
        justifyContent: "center",
        alignItems: "center",
    },
    divContainer: {
      flex:1,
    },
    imageAleatoire: {
        height: 200,
        width: 200,
        
    },
    lettreAleatoire: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    containerGage:{
        backgroundColor:"black",
        justifyContent: 'flex-end',
        paddingVertical: "10%"
    },
    text: {
        color: 'white',
        fontSize: 20,
        fontWeight: 700,
    },
    lettre : {
        color:"#FFFFFF",
    },
    button:{
        alignItems: 'center',
        justifyContent: 'center',
    },
    themeNom: {
        paddingTop: "30%",
        textAlign: "center",
        fontFamily: "Stora",
        fontSize: 50,
    }

});
export default RandomComponent;
