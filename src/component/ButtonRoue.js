import {StyleSheet, Image,Pressable,View,Modal,Text} from 'react-native';
import { useState } from 'react';
import { useNavigation } from '@react-navigation/native';



const ButtonRoue = ({image}) => {
    const navigation = useNavigation();

    
    const onPressJouer = () => {
      navigation.navigate('Roue');
    };

    return (
    <>
        <Pressable onPress={onPressJouer}>
            <Image style={{width: 60, height:60}} source={image}/>
        </Pressable>

    </>)
};

export default ButtonRoue;