const BackGroundThemeAleatoire = {
    '#18D5FF': require("../assets/images/fonds/bgtheme1.png"),
    '#FF0000': require("../assets/images/fonds/bgtheme2.png"),
    '#FFE602': require("../assets/images/fonds/bgtheme3.png"),
};

export default BackGroundThemeAleatoire;