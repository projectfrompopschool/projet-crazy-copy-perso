import * as React from 'react';
import { StyleSheet, Text, View, Button, ImageBackground, Image, Pressable } from 'react-native';

const Regles = ({ navigation }) => {
  const handlePress = () => {
    navigation.navigate('Game');
  };

  return (
    <>
      <Pressable onPress={handlePress}>
        <View style={styles.container}>
          <ImageBackground style={styles.background} source={require('../assets/images/fonds/regles1.png')}>
            <Text style={styles.appButtonText}>Règles</Text>
          </ImageBackground>
        </View>
      </Pressable>

      <View>
        <Text style={styles.heading}>Règles du jeu</Text>
      </View>

      <View>
        <Text style={styles.text}>Devine deux mots en rapport avec l’image qui commence par la lettre affichée, le premier à trouver a gagné.</Text>
      </View>

      <View>
        <Text style={styles.heading}>Façon de jouer</Text>
      </View>

      <View>
        <Text style={styles.text}>Compte jusqu’à 5 sur tes doigts, le premier qui arrive à ce score peut distribuer un gage.</Text>
      </View>

      <View style={styles.divImgJaune}>
        <Image style={styles.imgjaune} source={require('../assets/images/fonds/regles2.png')} />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    paddingTop: "27%",
  },
  background: {
    paddingTop: '20%',
    paddingBottom: '20%',
    marginHorizontal: '18%',
  },
  appButtonText: {
    fontFamily: 'Stora',
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 45,
    marginVertical: '4%',
  },
  text: {
    textAlign: 'center',
    fontSize: 22,
    paddingHorizontal: '5%',
    paddingBottom: '2%',
  },
  heading: {
    fontFamily: 'Stora',
    color: '#FF0000',
    textAlign: 'center',
    fontSize: 30,
    paddingTop: '6%',
  },
  imgjaune: {
    justifyContent: 'flex-end',
  },
  divImgJaune: {
    flex: 1,
    justifyContent: 'flex-end',
  },
});

export default Regles;